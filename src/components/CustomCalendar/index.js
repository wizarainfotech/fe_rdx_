import React from "react";
import DatePicker from "react-datepicker";
import moment from "moment";
import { Field as FormikField } from "formik";


const CustomCalender = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors, values }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  ...props
}) => (
    <div>
      <DatePicker
        {...field}
        {...props}
        selected={values[field.name]}
        onChange={val => {
          const value = val._d;
          let summa = moment(value).format('L')
          console.log("summa", summa)
          console.log("value", values[props.e.name])
          values[field.name] = val
        }}
      />
    </div >
  );


export default props => (
  <FormikField {...props} component={CustomCalender} />
);