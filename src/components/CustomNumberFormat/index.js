import React from "react";
import NumberFormat from 'react-number-format';
import { Field as FormikField } from "formik";

const CustomNumberFormat = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors, values, handleChange }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  ...props
}) => (
    <div>
      <NumberFormat
        // {...field}
        {...props}
        value={values.cellPhone}
        onValueChange={val => {
          const { formattedValue, value } = val;
          values.cellPhone = formattedValue
        }}
      />
    </div >
  );


export default props => (
  <FormikField {...props} component={CustomNumberFormat} />
);