import React from "react";
import ReactAutosuggest from "Components/ReactAutosuggest";
import { Field as FormikField } from "formik";

const CustomBootstrapInputComponent = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors, values }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  ...props
}) => (
    <div>
      <ReactAutosuggest
        {...field}
        {...props}
        initialValue={field.value}
        onChange={val => {
          values[props.e.name] = val
        }}
      />
    </div >
  );


export default props => (
  <FormikField {...props} component={CustomBootstrapInputComponent} />
);