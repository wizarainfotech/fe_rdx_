import React, { Component, Fragment } from "react";
import {
  Row,
  Button,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  Card,
  Input
} from "reactstrap";

import IntlMessages from "Util/IntlMessages";
import { Colxx, Separator } from "Components/CustomBootstrap";
import { BreadcrumbItems } from "Components/BreadcrumbContainer";

import PropertyGrid from "./details/propertyGrid.jsx";

import { Redirect } from "react-router-dom";

import { connect } from "react-redux";
import * as actionCreator from "Redux/propertyDetails/actions";
import * as apiCallCreator from "Redux/propertyDetails/_axios";
import {
  FORM_ADD,
  PROPERTY_GRID,
  LIEN_GRID,
  ASSESSEE_GRID,
  SEARCH_GRID,
  GET_SEARCH_DATA
} from "Constants/actionTypes";

class PropertyDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      searchArr: [
        {
          name: "propertyNumber",
          size: 3,
          type: "text",
          placeHolder: "Property Number"
        },
        {
          name: "name",
          size: 2,
          type: "text",
          placeHolder: "Name"
        },
        {
          name: "cellPhone",
          size: 3,
          type: "number",
          placeHolder: "Phone Number"
        },
        {
          name: "emailAddress",
          size: 2,
          type: "text",
          placeHolder: "Email",
        },
        {
          name: "address",
          size: 2,
          type: "text",
          placeHolder: "Address",
        }
      ],
    };
  }

  componentWillMount() {
    this.props.changeGrid(PROPERTY_GRID);
    // apiCallCreator.searchData(0, 10, {}, this.props.getAssesseeDatas);
  }

  render() {
    return (
      <Fragment >
        <div className="disable-text-selection">
          <Row>
            <Colxx xxs="12">
              <div className="mb-2">
                <h1>
                  <IntlMessages id="property.propertySearchDetails" />
                </h1>

                <div className="float-sm-right">
                  <div>
                    {this.renderRedirect()}
                    <Button
                      color="success"
                      size="lg"
                      className="default"
                      onClick={this.setRedirect}
                    >
                      <IntlMessages id="property.add-modal-title" />
                    </Button>
                  </div>
                  {"  "}
                </div>

                <BreadcrumbItems match={this.props.match} />
              </div>
              <div className="d-block d-md-inline-block">
                <UncontrolledDropdown className="float-md-left btn-group mb-1">
                  <DropdownToggle caret color="outline-dark" size="xs">
                    <IntlMessages id={this.setSelectedAction()} />
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem
                      onClick={() => this.props.changeGrid(PROPERTY_GRID)}
                      className={
                        this.props.propertyDetails.gridType == PROPERTY_GRID
                          ? "bg-primary"
                          : ""
                      }
                    >
                      <IntlMessages id="menu.propertyDetails" />
                    </DropdownItem>
                    <DropdownItem
                      onClick={() => this.props.changeGrid(LIEN_GRID)}
                      className={
                        this.props.propertyDetails.gridType == LIEN_GRID
                          ? "bg-primary"
                          : ""
                      }
                    >
                      <IntlMessages id="property.lienInfo" />
                    </DropdownItem>
                    <DropdownItem
                      onClick={() => this.props.changeGrid(ASSESSEE_GRID)}
                      className={
                        this.props.propertyDetails.gridType == ASSESSEE_GRID
                          ? "bg-primary"
                          : ""
                      }
                    >
                      <IntlMessages id="assessee.title" />
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </div>
              <Separator className="mb-3" />
            </Colxx>
          </Row>
          <Row>
            <Colxx xxs="12" className="mb-3">
              <Card className="d-flex flex-row">
                <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center" >
                  {this.fieldMapper(
                    this.state.searchArr
                  )}
                </div>
              </Card>
            </Colxx>
          </Row>
          <PropertyGrid />
        </div>
      </Fragment>
    );
  }


  fieldMapper = (arr) => {
    return arr.map((e, i) => {
      return (
        <Colxx xxs={e.size} key={i} className="mb-1">
          <Input type={e.type} name={e.name} placeholder={e.placeHolder} style={{ float: "left" }}
            onChange={val => {
              this.props.changeGrid(SEARCH_GRID);
              let c = {}
              let a = val.target.name
              c[a] = val.target.value
              if (this.props.propertyDetails.gridType == SEARCH_GRID) {
                apiCallCreator.searchData(
                  0,
                  10,
                  c,
                  this.props.searchData
                );
              }
            }}>
          </Input>
        </Colxx>
      )
    })
  }

  setSelectedAction = () => {
    if (this.props.propertyDetails.gridType == PROPERTY_GRID)
      return "menu.propertyDetails";
    if (this.props.propertyDetails.gridType == LIEN_GRID)
      return "property.lienInfo";
    if (this.props.propertyDetails.gridType == ASSESSEE_GRID)
      return "assessee.title";
    if (this.props.propertyDetails.gridType == SEARCH_GRID)
      return "menu.search";
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect push to="/app/propertyDetails/detailsform" />;
    }
  };

  setRedirect = () => {
    this.props.changeFormType(FORM_ADD);
    this.setState({
      redirect: true
    });
  };
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return {
    changeFormType: val => dispatch(actionCreator.ChangeFormType(val)),
    changeGrid: val => dispatch(actionCreator.ChangeGrid(val)),
    getAssesseeDatas: val => dispatch(actionCreator.GetAssesseeData(val)),
    searchData: val => dispatch(actionCreator.SearchData(val))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PropertyDetails);
